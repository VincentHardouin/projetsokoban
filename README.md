Ce projet a ete fait comme devoir pour l'universite de Tours.
Il a ete realise par Christian Mada-Mbari et moi meme. 

Jeu Sokoban, avec enregistrement de la progression du niveau en cours et des scores. 

Ce qui est implemente dans le programme : 
- Jeu sokoban 
- Enregistrement de la progression du niveau 
- Enregistrement du score 
- Reprise du niveau dont la progression a ete sauvegarde 
- Interface de connexion 
- Systeme de token/ cookie pour l'authentification automatique
- Connexion a une base de donnees grace a JDBC 



REQUIS pour le bon fonctionnement : 
- Importer le dump de la base de donnees 
- Modifier la class Connect en y mettant votre moyen de connexion a votre base de donnees. 

Utilise : 
- JDBC 
- JavaFX 


