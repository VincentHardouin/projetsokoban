package sokoban;

/**
 * Définition d'un niveau du jeu
 */
public class Niveau {
	private int numNiveau;
	private int pierres;
	private String terrain;
	private int score;

	public Niveau(int pierres, String terrain) {
		this.pierres = pierres;
		this.terrain = terrain;
	}
	
	public Niveau(int numNiveau, int score) {
		this.numNiveau = numNiveau;
		this.score = score;
	}
	
	public Niveau(int numNiveau, String terrain, int score) {
		this.numNiveau = numNiveau;
		this.terrain = terrain;
		this.score = score;
	}
	
	public Niveau() {
		
	}
	
	public int getPierres() {
		return pierres;
	}

	public String getTerrain() {
		return terrain;
	}

	public int getScore() {
		return score;
	}


	/**
	 * @return the numNiveau
	 */
	public int getNumNiveau() {
		return numNiveau;
	}


	/**
	 * @param numNiveau the numNiveau to set
	 */
	public void setNumNiveau(int numNiveau) {
		this.numNiveau = numNiveau;
	}


	/**
	 * @param pierres the pierres to set
	 */
	public void setPierres(int pierres) {
		this.pierres = pierres;
	}


	/**
	 * @param terrain the terrain to set
	 */
	public void setTerrain(String terrain) {
		this.terrain = terrain;
	}


	/**
	 * @param score the score to set
	 */
	public void setScore(int score) {
		this.score = score;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Niveau [numNiveau=" + numNiveau + ", pierres=" + pierres + ", terrain=" + terrain + ", score=" + score
				+ "]";
	}
	
	

}
