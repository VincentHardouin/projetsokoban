package sokoban;

import sokoban.elementjeu.AbstractElementJeu;

import java.io.IOException;
import java.sql.SQLException;

import connexion.GestionToken;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * Initialisation de l'interface graphique et de la classe JeuSokoban
 */
public class Main extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws SQLException, IOException {
    	
    	
    	GestionToken gt = new GestionToken(); 
		gt.read();
    	
    	
        Pane gamePane = new Pane();

        StackPane stackPane = new StackPane(gamePane);
        int w = Niveaux.gameWidth * AbstractElementJeu.width;
        int h = Niveaux.gameHeight * AbstractElementJeu.width;
        stackPane.setMinSize(w, h);
        stackPane.setMaxSize(w, h);
        
        Button actionButton = new Button("Se connecter");
        actionButton.setFocusTraversable(false);
        Label nameLabel = new Label("Test");
        nameLabel.setPadding(new Insets(5));
        HBox topPane1 = new HBox(); 
        topPane1.setPadding(new Insets(5));
        topPane1.getChildren().add(actionButton);
        topPane1.getChildren().add(nameLabel);
        
        Button btnUndo = new Button("Annuler dépl.");
        btnUndo.setFocusTraversable(false);
        Button btnJeu = new Button("Réinit. jeu");
        btnJeu.setFocusTraversable(false);
        Button btnNiveau = new Button("Réinit. niveau");
        btnNiveau.setFocusTraversable(false);
        HBox topPane = new HBox(5, btnUndo, btnJeu, btnNiveau);
        topPane.setPadding(new Insets(5));
        Label label = new Label();
        label.setPadding(new Insets(5));
        Button classementBtn = new Button("Classement");
        classementBtn.setFocusTraversable(false);
        HBox bottomPane = new HBox(classementBtn);
        bottomPane.setPadding(new Insets(5));
        
        VBox vbox = new VBox();
        vbox.getChildren().add(topPane1);
        vbox.getChildren().add(topPane);
        vbox.getChildren().add(stackPane);
        vbox.getChildren().add(label);
        vbox.getChildren().add(bottomPane);
        
        Scene scene = new Scene(vbox);
        

        new JeuSokoban(stage,gamePane, label, btnUndo, btnJeu, btnNiveau, actionButton, nameLabel, classementBtn);

        stage.setScene(scene);
        stage.setTitle("Sokoban");
        stage.show();

    }

}
