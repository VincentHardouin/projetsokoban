package sokoban;

import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import sokoban.action.AbstractAction;
import sokoban.action.DeplacementPierre;
import sokoban.action.DeplacementSimple;
import sokoban.action.EliminationPierre;
import sokoban.classement.ClassementView;
import sokoban.elementjeu.Joueur;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.DialogPane;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import javafx.stage.WindowEvent;

/**
 * Gère le déroulement du jeu
 */
public class JeuSokoban {
	private final Pane gamePane;
	private final Label label;

	private int errors;

	private int niveau;
	private int nbrDeplacement;
	private TerrainSokoban terrainSokoban;
	private int pierresRestantes;

	private Button actionBtn;
	private Label nameLabel;
	private boolean recommence = false;
	private Stage stage;

	private LinkedList<AbstractAction> actions = new LinkedList<>();
	private JeuSokobanController controller; 

	private boolean inSaveMode = false;

	JeuSokoban(Stage stage,Pane gamePane, Label label, Button btnUndo, Button btnJeu, Button btnNiveau, Button actionBtn,
			Label nameLabel, Button classementBtn) throws SQLException {
		niveau = 0;
		this.gamePane = gamePane;
		terrainSokoban = new TerrainSokoban(gamePane);
		this.label = label;
		this.nameLabel = nameLabel;
		this.actionBtn = actionBtn;
		this.stage = stage;

		controller = new JeuSokobanController();

		actionBtnUndo(btnUndo);
		actionBtnJeu(btnJeu);
		actionBtnNveau(btnNiveau);
		actionGamePane(gamePane);
		labelActionBtn();
		actionActionBtn(actionBtn);
		actionClassementBtn(gamePane, classementBtn);
		actionFermeture();
		initNiveau(niveau);

		if(controller.isConnect()) if(controller.haveSave()) requestInitSave();
	}


	/**
	 * Permets de mettre un event handler sur le bouton undo, permettant d'annuler une action
	 * @param btnUndo
	 */
	private void actionBtnUndo(Button btnUndo) {
		btnUndo.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				if (!actions.isEmpty()) {

					AbstractAction c = actions.removeLast();
					c.executeDefait(terrainSokoban);
					if (c instanceof DeplacementSimple || c instanceof DeplacementPierre
							|| c instanceof EliminationPierre) {
						if (nbrDeplacement >= 5)
							nbrDeplacement = nbrDeplacement + 5;
						else if (nbrDeplacement >= 30)
							nbrDeplacement = nbrDeplacement + 10;
						majLabel();
					}
					if (c instanceof EliminationPierre) {
						pierresRestantes++;

						majLabel();
					}
				}
			}
		});
	}

	
	/**
	 * Permets d'avoir un event handler sur le bouton jeu
	 * qui remets le jeu à 0
	 * @param btnJeu
	 */
	private void actionBtnJeu(Button btnJeu) {
		btnJeu.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				niveau = 0;
				initNiveau(niveau);
			}
		});
	}
	
	/**
	 * Permets d'avoir un event handler sur le bouton renitialisation niveau 
	 * qui remets le niveau à 0
	 * @param btnNiveau
	 */
	private void actionBtnNveau(Button btnNiveau) {
		btnNiveau.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				initNiveau(niveau);
			}
		});
	}

	/**
	 * Permets de définir un Event handler grâce à l'appuie des touches 
	 * @param gamePane
	 */
	private void actionGamePane(Pane gamePane) {
		gamePane.getScene().setOnKeyPressed(new EventHandler<KeyEvent>() {
			@Override
			public void handle(KeyEvent event) {
				switch (event.getCode()) {
				case LEFT:
					try {
						deplacement(new Coord(-1, 0));
					} catch (NoSuchAlgorithmException e) {
						e.printStackTrace();
					}
					break;
				case RIGHT:
					try {
						deplacement(new Coord(1, 0));
					} catch (NoSuchAlgorithmException e) {
						e.printStackTrace();
					}
					break;
				case UP:
					try {
						deplacement(new Coord(0, -1));
					} catch (NoSuchAlgorithmException e) {
						e.printStackTrace();
					}
					break;
				case DOWN:
					try {
						deplacement(new Coord(0, 1));
					} catch (NoSuchAlgorithmException e) {
						e.printStackTrace();
					}
					break;
				default:
					break;
				}
			}
		});
	}

	/**
	 * Permets de définir un event Handler pour le button action 
	 * Celui-ci permets de connecter ou déconnecter une personne
	 * @param actionBtn
	 */
	private void actionActionBtn(Button actionBtn) {
		actionBtn.setOnAction(e -> {
			if (controller.clearJoueurAuth()) {
				labelActionBtn();
				System.out.println("Deconnexion joueur");
			} else {
				requestLogInOrSignIn();
			}
		});
	}
	
	/**
	 * Permets de changer de scene pour aller sur la scene de classement 
	 * @param gamePane
	 * @param classementBtn
	 */
	private void actionClassementBtn(Pane gamePane, Button classementBtn) {
		classementBtn.setOnAction(e -> {
			if(nbrDeplacement != 0) {				
				int reponse = requestSaveProgression();
				switch(reponse) {
				case 1: 
					Niveau n = new Niveau(niveau, terrainSokoban.getNiveau(), nbrDeplacement);
					controller.saveProgression(n);
					break;
				case 2: 
					
					break;
				case 3: 
					e.consume();
					break;
				default: break;
				}
			}
			try {
				new ClassementView(((Stage) gamePane.getScene().getWindow()));
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		});
	}

	
	/**
	 * Permets de mettre un eventHandler sur la detection de la fermeture 
	 * Ce qui permets de demander à sauvegarder ou non et fermer la connexion
	 */
	private void actionFermeture() {
		stage.setOnCloseRequest(new EventHandler<WindowEvent>() {

			@Override
			public void handle(WindowEvent event) {
				do {
					errors = 0;
					int reponse = requestSaveProgression();
					switch(reponse) {
					case 1: 
						niveau++;
						Niveau n = new Niveau(niveau, terrainSokoban.getNiveau(), nbrDeplacement);
						if(!controller.isConnect()) {
							if(requestLogInOrSignIn()){
								controller.saveProgression(n);

							} else errors++;
							
						} else controller.saveProgression(n);

						break;
					case 2: 					
						((Stage) gamePane.getScene().getWindow()).close();
						break;
					case 3: 
						event.consume();
						break;
					default: break;
					}
				} while(errors!=0);
				
				controller.close();
			}

		});
	}

	/**
	 * Gère une commande de déplacement
	 * @throws NoSuchAlgorithmException 
	 */
	public void deplacement(Coord depl) throws NoSuchAlgorithmException {
		Optional<AbstractAction> optionCommande = terrainSokoban.prepareDeplacement(depl);

		if (optionCommande.isPresent()) {
			AbstractAction c = optionCommande.get();
			actions.addLast(c);
			c.execute(terrainSokoban);
			if (c instanceof DeplacementSimple || c instanceof DeplacementPierre) {
				nbrDeplacement++;
				majLabel();
			}

			else if (c instanceof EliminationPierre) {
				pierresRestantes--;
				nbrDeplacement++;

				majLabel();

				if (pierresRestantes == 0) {
					niveauSuivant();
				}
			}

		}
	}

	/**
	 * Permets l'initialisation du niveau founi en paramètre
	 * @param niveau : int : numéro du niveau que l'on souhaite initialiser
	 */
	private void initNiveau(int niveau) {
		actions.clear();
		terrainSokoban.initNiveau(Niveaux.getNiveau(niveau).getTerrain());
		pierresRestantes = Niveaux.getNiveau(niveau).getPierres();
		nbrDeplacement = 0;
		majLabel();
	}
	
	/**
	 * Permets l'initialisation d'un niveau récupérer par une sauvegarde dans la base de données
	 */
	private void initSaveNiveau() {
		Niveau n = controller.getSauvegarde();
		actions.clear();
		terrainSokoban.initNiveau(n.getTerrain());
		pierresRestantes = terrainSokoban.getNbrPierresRestantes();
		nbrDeplacement = n.getScore();
		majLabel();
	}
	
	/**
	 * Permets de mettre à jour les label du jeu 
	 */
	private void majLabel() {
		label.setText(String.format("Niveau : %d - Pierres à éliminer : %d - Score : %d", niveau + 1,
				pierresRestantes, nbrDeplacement));
	}
	
	/**
	 * Permets de passer au niveau suivant 
	 * Propose d'enregistrer ou non le score
	 * @throws NoSuchAlgorithmException
	 */
	private void niveauSuivant() throws NoSuchAlgorithmException {
		if (niveau == Niveaux.nombreNiveau() - 1) {
			new Alert(Alert.AlertType.INFORMATION, "Vous avez terminé tous les niveaux").showAndWait();
		} else {
			niveau++;
			new Alert(Alert.AlertType.INFORMATION, "Vous avez terminé le niveau " + niveau).showAndWait();
		}
		if (choiceSaveOrNot()) {
			Niveau n = new Niveau(niveau, nbrDeplacement);
			controller.saveScore(n);
		}

		initNiveau(niveau);
	}
	
	
	/**
	 * Ouvre une alert proposant le choix d'enregistrer ou non 
	 * @return 
	 */
	private boolean choiceSaveOrNot() {
		int erreurConnexion = 1;
		inSaveMode = true;
		while (erreurConnexion == 1) {
			erreurConnexion = 1;
			Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
			if(controller.haveBetterScore(niveau,nbrDeplacement)){
				int oldScore = controller.getOldScore(niveau); 
				alert = new Alert(Alert.AlertType.CONFIRMATION,
						"Votre ancien score est de : "+ oldScore + " contre " + nbrDeplacement + " maintenant \n"
								+ "Voulez-vous sauvegarder votre progression pour ce niveau " + niveau + " ?", ButtonType.OK,
								ButtonType.CANCEL);	
			} else {
				alert  = new Alert(Alert.AlertType.CONFIRMATION,
						"Voulez-vous sauvegarder votre progression pour ce niveau " + niveau, ButtonType.OK,
						ButtonType.CANCEL);	
			}

			if (alert.showAndWait().get() == ButtonType.OK) {
				if(!controller.isConnect()) {
					if(requestLogInOrSignIn()) erreurConnexion = 0;
					else erreurConnexion = 1;
				} else {
					erreurConnexion = 0;
				}
				return true;
			} else return false;

		}
		return false;

	}
	
	/**
	 * Ouvre une alert proposant le choix de connexion : Se connecter ou s'inscrire
	 * @return
	 */
	private boolean requestLogInOrSignIn() {
		do {
			recommence = false;
			Alert alert1 = new Alert(AlertType.CONFIRMATION);
			alert1.setTitle("Choix de connexion");
			alert1.setHeaderText("Veuillez vous inscrire ou vous connecter");
			alert1.setContentText("");

			ButtonType buttonTypeOne = new ButtonType("Connexion");
			ButtonType buttonTypeTwo = new ButtonType("Inscription");
			ButtonType buttonTypeCancel = new ButtonType("Cancel", ButtonData.CANCEL_CLOSE);

			alert1.getButtonTypes().setAll(buttonTypeOne, buttonTypeTwo, buttonTypeCancel);

			Optional<ButtonType> result = alert1.showAndWait();

			if (result.get() == buttonTypeOne) {
				if (formLogin()) return true;

			} else if (result.get() == buttonTypeTwo) {
				if (formRegister()) return true;

			} else return false;

		} while(recommence);
		return false;
	}
	
	
	/**
	 * Ouvre un dialog permettant à l'utilisateur de fournir ses identifiants de connexion
	 * @return
	 */
	private Boolean formLogin() {
		List<ButtonType> list = new ArrayList<ButtonType>();

		do {
			errors = 0;
			Dialog<Joueur> dialog = new Dialog<>();
			dialog.setTitle("Connexion");
			dialog.setHeaderText("Veuillez remplir le formulaire :");

			DialogPane dialogPane = dialog.getDialogPane();
			dialogPane.getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);
			TextField textField = new TextField("Pseudo ou Email");
			PasswordField passwordField = new PasswordField();
			passwordField.setPromptText("Password");

			dialogPane.setContent(new VBox(8, textField, passwordField));

			Platform.runLater(textField::requestFocus);
			dialog.setResultConverter((ButtonType button) -> {
				if (button == ButtonType.OK) {
					list.add(ButtonType.OK);
					return new Joueur(textField.getText(), passwordField.getText());
				} else if (button == ButtonType.CANCEL) {
					list.add(ButtonType.CANCEL);
					dialog.close();
					recommence = true;

				}

				return null;
			});
			Optional<Joueur> optionalResult = dialog.showAndWait();
			optionalResult.ifPresent((Joueur userResult) -> {
				try {
					if(!controller.loginJoueur(userResult.getEmail(), userResult.getPassword())) errors++;

					if (errors != 0) {
						Alert alerts = new Alert(AlertType.ERROR);
						alerts.setTitle("Erreur mot de passe");
						alerts.setHeaderText("Votre mot de passe est incorrect");
						alerts.setContentText("Veuillez recommencer");

						alerts.showAndWait();
					}
				} catch (SQLException e) {

					e.printStackTrace();
				} catch (NoSuchAlgorithmException e) {

					e.printStackTrace();
				}

			});

		} while (errors != 0);

		if(!inSaveMode) if(controller.haveSave()) requestInitSave(); 

		inSaveMode = false;
		labelActionBtn();


		if (list.get(0) == ButtonType.OK) {
			return true;
		} else {
			return false;
		}

	}
	
	/**
	 * Ouvre un dialog permettant à l'utilisateur de s'inscrire 
	 * @return
	 */
	private Boolean formRegister() {
		List<ButtonType> list2 = new ArrayList<ButtonType>();
		do {
			errors = 0;
			Dialog<Joueur> dialog = new Dialog<>();
			dialog.setTitle("Inscription");
			dialog.setHeaderText("Veuillez remplir le formulaire :");

			DialogPane dialogPane = dialog.getDialogPane();
			dialogPane.getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);

			TextField textField2 = new TextField();
			textField2.setPromptText("Pseudo");
			TextField textField = new TextField();
			textField.setPromptText("Email");
			PasswordField passwordField = new PasswordField();
			passwordField.setPromptText("Password");

			dialogPane.setContent(new VBox(8, textField2, textField, passwordField));

			Platform.runLater(textField2::requestFocus);
			dialog.setResultConverter((ButtonType button) -> {
				if (button == ButtonType.OK) {
					// converti les résultats en joueur
					list2.add(ButtonType.OK);
					String passwordSaisieHash = controller.hashPassword(passwordField.getText());
					return new Joueur(textField2.getText(), textField.getText(), passwordSaisieHash);

				} else {
					list2.add(ButtonType.CANCEL);
					recommence = true;
					return null;
				}
			});
			Optional<Joueur> optionalResult = dialog.showAndWait();
		

			optionalResult.ifPresent((Joueur userResult) -> {

				try {
					boolean ok = true;
					if (ok) {
						if(controller.verifExistByEmail(userResult)) {
							errors = 1; 
						}  
						if(controller.verifExistByPseudo(userResult)) {
							errors = 2; 
						}
						
					}
					if (errors != 0) {
						
						switch(errors) {
						case 1: 
							Alert alert = new Alert(AlertType.ERROR); 
							alert.setTitle("Problème d'identifiants");
							alert.setHeaderText("Email existant");
							alert.setContentText("Votre email existe déjà");
							alert.showAndWait();
							break;
						
						case 2:
							Alert alert2 = new Alert(AlertType.ERROR); 
							alert2.setTitle("Problème d'identifiants");
							alert2.setHeaderText("Pseudo existant");
							alert2.setContentText("Votre pseudo existe déjà");
							alert2.showAndWait();
							break;
						}
						
					
					} else {
						if(!controller.newUser(userResult)) {
							Alert alert = new Alert(AlertType.ERROR); 
							alert.setTitle("Veuillez remplir toules champs");
							alert.setHeaderText("Erreur de saisie");
							alert.setContentText("Veuillez recommencer");
							alert.showAndWait();
						}
					}
				} catch (SQLException e) {

					e.printStackTrace();
				}
			});
		} while (errors != 0);

		labelActionBtn();
		if(!inSaveMode) if(controller.haveSave()) requestInitSave();

		inSaveMode = false;
		if(list2.get(0) == ButtonType.OK) {
			return true;
		} else {
			return false;
		}
	}
	
	
	/**
	 * Affiche une alert pour demander à l'utilisateur s'il souhaite récupérer sa progression ou non
	 */
	private void requestInitSave() {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Confirmation Récupérer la sauvegarde");
		alert.setContentText("Voulez-vous récupérer votre sauvegarde ?");

		Optional<ButtonType> result = alert.showAndWait();
		if (result.get() == ButtonType.OK) {
			initSaveNiveau();
		} 
	}
	
	
	/**
	 * Permets de mettre à jour le label du bouton Action (connexion/deconnexion)
	 */
	private void labelActionBtn() {
		if (controller.isConnect()) {
			actionBtn.setText("Deconnexion");
			nameLabel.setText(controller.getNomJoueur());
		} else {
			actionBtn.setText("Se connecter");
			nameLabel.setText("");
		}
	}

	/**
	 * Permets d'afficher un dialog demandant à l'utilisateur s'il souhaite enregistré sa progression ou non
	 * @return int :
	 * <ul>
	 * 	<li>0 : S'il s'agit du dernier niveau et donc qu'il n'a pas le droit d'enregistrer sa progression </li>
	 * 	<li>1 : s'il le souhaite</li>
	 * 	<li>2 : s'il ne le souhaite pas</li>
	 * 	<li>3 : appuie sur annuler ou ferme la boite d'alert</li>
	 * </ul> 
	 */
	private int requestSaveProgression() {
		if(niveau < Niveaux.nombreNiveau()) {	
			inSaveMode = true;
			Alert alert = new Alert(AlertType.CONFIRMATION);
			alert.setTitle("Confirm Exit");
			alert.setHeaderText("Voulez-vous sauvegarder votre avancement ?");
			alert.setContentText("");
			
			ButtonType buttonTypeOne = new ButtonType("Oui");
			ButtonType buttonTypeTwo = new ButtonType("Non");
			ButtonType buttonTypeCancel = new ButtonType("Cancel", ButtonData.CANCEL_CLOSE);
			
			alert.getButtonTypes().setAll(buttonTypeOne, buttonTypeTwo, buttonTypeCancel);
			
			Optional<ButtonType> result = alert.showAndWait();
			if (result.get() == buttonTypeOne) return 1;
			else if (result.get() == buttonTypeTwo) return 2;
			else return 3;
		} else return 0;

	}

}
