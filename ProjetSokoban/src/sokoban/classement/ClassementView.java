package sokoban.classement;

import java.io.IOException;
import java.sql.SQLException;

import connexion.GestionRqt;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import sokoban.Main;
import sokoban.elementjeu.Joueur;


/**
 * Vue pour l'affichage du classement
 *
 */
public class ClassementView {
	
	private Stage stage;
	private Button returnBtn; 
	
	
	private TableView<Joueur> classementTV;
	private TableColumn<Joueur, Integer> classementCol;
	private TableColumn<Joueur, String> nameCol; 
	private TableColumn<Joueur, Integer> scoreCol;
	
	private Button niv1Btn; 
	private Button niv2Btn; 
	private Button niv3Btn; 
	private Button niv4Btn; 
	private Button niv5Btn; 
	private Button niv6Btn; 
	private Button niv7Btn; 
	
	private GestionRqt g;
	private ObservableList<Joueur> listeJoueurs;
	private ClassementController controller;
	
	public ClassementView(Stage stage) throws SQLException {
		
		g = new GestionRqt(); 
		
		controller = new ClassementController();
		
		this.stage = stage;
		initialisation();
		
		actionReturnBtn(stage);
		actionNivBtn();
		actionFermeture();
		
	}

	/**
	 * Permets d'ajout un eventhandler auw boutons de choix de niveau 
	 */
	private void actionNivBtn() {
		EventHandler<ActionEvent> event = new EventHandler<ActionEvent>() { 

			public void handle(ActionEvent e) {
				Button b = (Button) e.getSource();
				listeJoueurs = FXCollections.observableArrayList(controller.getJoueursScoreByLevel(b.getText()));
				classementTV.setItems(listeJoueurs);
			} 

		}; 

		niv1Btn.setOnAction(event);
		niv2Btn.setOnAction(event);
		niv3Btn.setOnAction(event);
		niv4Btn.setOnAction(event);
		niv5Btn.setOnAction(event);
		niv6Btn.setOnAction(event);
		niv7Btn.setOnAction(event);
	}
	
	/**
	 * Ajoute un eventhandler à la fermeture 
	 */
	private void actionFermeture() {
		stage.setOnCloseRequest(e -> {
			controller.close();
			Platform.exit();
		});
	}


	/**
	 * Ajoute un eventhandler sur bouton return
	 * Permets de revenir au jeu 
	 * @param stage
	 */
	private void actionReturnBtn(Stage stage) {
		returnBtn.setOnAction(e -> {
			try {
				new Main().start(stage);
			} catch (SQLException | IOException e1) {
				
				e1.printStackTrace();
			}
		});
	}
	
	
	
	/**
	 * Permets l'initialisation de la vue
	 */
	private void initialisation() {
		listeJoueurs = FXCollections.observableArrayList(g.getJoueursScoreByLevel(1));
		
		returnBtn = new Button("Retour"); 
		
		niv1Btn = new Button("1");
		niv2Btn = new Button("2");
		niv3Btn = new Button("3");
		niv4Btn = new Button("4");
		niv5Btn = new Button("5");
		niv6Btn = new Button("6");
		niv7Btn = new Button("7");
		

		HBox emplacementLevelBtn = new HBox();
		emplacementLevelBtn.getChildren().addAll(niv1Btn,niv2Btn,niv3Btn,niv4Btn,niv5Btn,niv6Btn,niv7Btn);
		
		classementTV = new TableView<Joueur>();
		classementCol = new TableColumn<Joueur, Integer>("classement");
		nameCol = new TableColumn<Joueur, String>("joueur"); 
		scoreCol = new TableColumn<Joueur, Integer>("score");
		
		classementTV.getColumns().add(classementCol);
		classementTV.getColumns().add(nameCol);
		classementTV.getColumns().add(scoreCol);
		
		Label titre = new Label("Classement"); 
		titre.setFont(new Font("Arial",24));
		
		VBox center = new VBox(); 
		center.getChildren().addAll(titre,classementTV,emplacementLevelBtn);
		
		
		BorderPane borderPane = new BorderPane(); 
		
		borderPane.setTop(returnBtn);
		BorderPane.setMargin(returnBtn, new Insets(5));

		borderPane.setCenter(center);
		BorderPane.setMargin(center, new Insets(20));
		
		Scene scene = new Scene(borderPane);
		stage.setScene(scene);
		
		classementCol.setCellFactory(col -> {
		    TableCell<Joueur, Integer> cell = new TableCell<>();
		    cell.textProperty().bind(Bindings.createStringBinding(() -> {
		        if (cell.isEmpty()) {
		            return null ;
		        } else {
		            return Integer.toString(cell.getIndex()+1);
		        }
		    }, cell.emptyProperty(), cell.indexProperty()));
		    return cell ;
		});
		
		nameCol.setCellValueFactory(
				new PropertyValueFactory<Joueur, String>("nomJoueur"));
		scoreCol.setCellValueFactory(
				new PropertyValueFactory<Joueur, Integer>("score"));
		
		classementTV.setItems(listeJoueurs);
	}

}
