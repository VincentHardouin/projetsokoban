package sokoban.classement;

import java.sql.SQLException;
import java.util.List;

import connexion.GestionRqt;
import sokoban.elementjeu.Joueur;


/**
 * Controller pour le classement
 *
 */
public class ClassementController {

	private GestionRqt g = new GestionRqt(); 
	
	public ClassementController() throws SQLException {
		g = new GestionRqt(); 
	}

	/**
	 * Permets de récupérer la liste trier des scores pour le niveau choisi
	 * @param text : String : texte contenant le numéro du niveau 
	 * @return List<Joueur> : classement trié des scores pour ce niveau
	 */
	public List<Joueur> getJoueursScoreByLevel(String text) {
		int level = Integer.parseInt(text); 
		List<Joueur> list = g.getJoueursScoreByLevel(level);
		return list;
	}
	
	/**
	 * Permets de fermer la connexion
	 */
	public void close() {
		g.closeGestionRqt();
	}
	
}
