package sokoban;

/**
 * Coordonnées sur le plateau de jeu
 */
public class Coord implements Comparable<Coord> {

    private int x;
    private int y;

    public Coord(int x, int y) {

        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    /**
     * Additionne deux coordonnées
     * @param coord à ajouter à l'objet courant
     * @return un nouvel objet Coord
     */
    public Coord add(Coord coord) {
        return new Coord(this.x + coord.x, this.y + coord.y);
    }
    
    
    /**
     * Compare les coord d'abord par leur y ensuite par leur x; 
     *
     */
	@Override
	public int compareTo(Coord o) {
		if(this.y < o.y) return -1; 
		else if((this.y == o.y) && this.x < o.x) return -1; 
		else if(this.y == o.y && this.x == o.x) return 0;
		return 1; 
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Coord [x=" + x + ", y=" + y + "]";
	}
	
	

}
