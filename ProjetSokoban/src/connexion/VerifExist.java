package connexion;

import java.sql.SQLException;

import sokoban.elementjeu.Joueur;

/**
 * Permets de vérifier l'existance d'un Joueur dans la bd à l'aide de son email et pseudo
 *
 */
public class VerifExist {
	
	private Joueur j; 
	private GestionRqt g;
	
	public VerifExist(Joueur j) throws SQLException {
		g = new GestionRqt();
		this.j = j;
	}
	
	public VerifExist(String text) throws SQLException {
		g = new GestionRqt();
		this.j = new Joueur(text, text, "");
		
	}
	
	/**
	 * Permets de vérifier le joueur de l'instance à l'aide de l'email
	 * @return boolean : true s'il existe ;false sinon
	 * @throws SQLException
	 */
	public boolean verifEmail() throws SQLException {
		Joueur joueurRecup = g.getJoueur(j.getEmail());
		if(joueurRecup == null) return false;
		return true;
	}
	
	/**
	 * Permets de vérifier le joueur passer dans le constructeur à l'aide du pseudo 
	 * @return boolean : true s'il en existe déjà un avec ce pseudo; false sinon 
	 * @throws SQLException
	 */
	public boolean verifPseudo() throws SQLException {
		Joueur joueurRecup = g.getJoueurByPseudo(j.getNomJoueur());
		if(joueurRecup == null) return false;
		return true;
	}

}
