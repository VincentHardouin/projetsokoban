package connexion;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;


/**
 * Permets de générer des tokens
 *
 */
public class TokenGen {

	private int hasard;
	private StringBuilder c;
	private List<Integer> l;

	public TokenGen() {
		this.l = new ArrayList<>();
		this.c = new StringBuilder();

	}
	
	/**
	 * Méthode retournant un token qui a été généré aléatoirement parmis certains caractères ascii 
	 * 
	 * @return String : token généré d'une longueur de 20 caractères
	 */
	public String generateToken() {

		for (int i = 33; i < 122; i++) {
			l.add(i);
		}

		c.delete(0, 10); 


		int nbr = 0; 
		do {
			hasard = l.get(new SecureRandom().nextInt(89));
			c.append((char) hasard);
			nbr++;
		} while(nbr < 20); 

		return c.toString();

	}


}
