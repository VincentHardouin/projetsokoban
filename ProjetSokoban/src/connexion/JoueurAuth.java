package connexion;

import java.sql.SQLException;

import sokoban.elementjeu.Joueur;


/**
 * Class permettant de faire un singleton 
 * Celui-ci stocke le joueur connecté à l'application 
 *
 */
public class JoueurAuth {
	
	private static Joueur joueur; 
	private static Boolean set = false;
	
	
	/**
	 * Permets d'instancier le joueur connecté 
	 * Si un joueur a déjà été instancié avant le nouveau ne sera pas pris en compte 
	 * comme il s'agit d'un singleton 
	 * 
	 * @param joueur : Joueur 
	 * @throws SQLException
	 */
	public JoueurAuth(Joueur joueur) throws SQLException {
		if(!set) {			
			JoueurAuth.joueur = joueur;			
			set = true;
		}
	}
	
	
	public JoueurAuth() {
		
	}
	
	/**
	 * Permets de récupérer le joueur instancié, soit le joueur connecté
	 * @return Joueur
	 */
	public Joueur getJoueurAuth() {
		return joueur;
	}
	
	
	/**
	 * Permets de pouvoir instancier à nouveau ce singleton 
	 */
	public void clear() {
		JoueurAuth.joueur = null; 
		JoueurAuth.set = false;
	}
	

}
