package connexion;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;


/**
 * Class permettant la gestion du token 
 * Le token permets de garder un utilisateur connecter même après avoir relancer l'application 
 * Le token : 
 * <ul> 
 * 	<li>Est à usage unique : change à chaque utilisation</li>
 * 	<li>Est stocké dans un fichier text</li>
 * 	<li>Le fichier text est vidé si l'user utilise le bouton de deconnexion</li>
 * </ul>
 * @author vincenthardouin
 *
 */
public class GestionToken {
	
	private GestionRqt g; 
	
	public GestionToken() throws SQLException {
		g = new GestionRqt(); 
	}
	
	/**
	 * Permets de lire le fichier text content le token </br></br>
	 * Comportement : </br>
	 * 1 - SI le fichier text contient un token : 
	 * <ul>
	 * 	<li>Vérifie si le token appartient à un user dans la bd</li>
	 * 	
	 * 		<ul>
	 *			<li>Si oui :</li>
	 *			<ol>
	 * 				<li>Récupère l'user</li>
	 * 				<li>On identifie l'user dans l'application</li>
	 * 				<li>Genere un nouveau</li>
	 * 			</ol>
	 * 			<li>Sinon on ne fait rien</li>
	 * 		</ul>
	 * 	
	 * </ul>
	 * 2 - Si le fichier n'a pas token on ne fait rien
	 * 
	 * 
	 * @throws SQLException
	 * @throws IOException
	 */
	public void read() throws SQLException, IOException {
		String s = "token.txt";
		BufferedReader entree = null;
		FileReader f;
		String token = "";
		try {
			f = new FileReader(s);
			entree = new BufferedReader(f);
			token = entree.readLine();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			entree.close();
		}
		
		if(token != null) {
			if(authWithToken(token)) newToken();
		}
	
	}
	
	
	/** 
	 * Permets de vérifier que le token correspond à un joueur
	 * S'il appartient à un jour on instancie le singleton JoueurAuth pour que celui-ci soit 
	 * connecté sur toute l'application et on retourne true 
	 * Sinon on retourne false
	 * 
	 * @param token String : token correspondant potentiellement à un joueur dans la bd
	 * @return boolean : true si un joueur a été authentifié; false sinon 
	 * @throws SQLException
	 */
	private boolean authWithToken(String token) throws SQLException {
		int idJoueur = g.getJoueurToken(token);
		if(idJoueur != 0) {
			System.out.println("Token valide");
			new JoueurAuth(g.getJoueur(idJoueur));
			return true;
		} else {
			System.out.println("Token non valide pas d'auth");
			return false;
		}
	}
	
	/**
	 * Permets de renouveler le token dans la bd et dans le fichier text 
	 * pour le prochain lancement de l'application
	 */
	public void newToken() {
		TokenGen t = new TokenGen(); 
		String token = t.generateToken();
		
		if(g.setToken(token)) {
			PrintWriter sortie = null;
			FileWriter f;
			try {
				f = new FileWriter("token.txt");
				BufferedWriter buffer = new BufferedWriter(f);
				sortie = new PrintWriter(buffer);
				sortie.println(token);
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				sortie.close();
			}
		}
	}
	
	/**
	 * Permets de vider le fichier text pour pas que l'user soit authentifié 
	 * au prochain démarrage de l'application
	 */
	public void clear() {
		PrintWriter sortie = null;
		FileWriter f;
		try {
			f = new FileWriter("token.txt");
			BufferedWriter buffer = new BufferedWriter(f);
			sortie = new PrintWriter(buffer);
			sortie.println("");
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			sortie.close();
		}
	}
	
}
